import { ref } from "vue";

let MyTasks = ref([]);
let FinishedTasks = ref([]);
let DeletedTasks = ref([]);
let IndexPage = ref([]);
export { MyTasks, FinishedTasks, DeletedTasks, IndexPage };
